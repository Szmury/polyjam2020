﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject Buttons;
    public GameObject Creators;
    
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowButtons()
    {
        Buttons.gameObject.SetActive(true);
        Creators.gameObject.SetActive(false);
    }
    
    public void HideButtons()
    {
        Buttons.gameObject.SetActive(false);
        Creators.gameObject.SetActive(true);
    }

    public void LoadScene()
    {
        SceneManager.LoadScene("mainScene", LoadSceneMode.Single);
    }
}
