﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] public float sensX = 25;
    [SerializeField] public int scrollDistance = 25;
    [SerializeField] public float scrollSpeed = 50;

    [SerializeField] public LayerMask groundMask;

    void Start()
    {
    }

    public string zoomingAxis = "Mouse ScrollWheel";

    private float ScrollWheel
    {
        get { return Input.GetAxis(zoomingAxis); }
    }

    private Vector2 MouseInput
    {
        get { return Input.mousePosition; }
    }

    public float maxHeight = 20f;
    public float minHeight = 15f;

    void Update()
    {
        Vector3 desiredMove = new Vector3();

        Rect leftRect = new Rect(0, 0, scrollDistance, Screen.height);
        Rect rightRect = new Rect(Screen.width - scrollDistance, 0, scrollDistance, Screen.height);
        Rect upRect = new Rect(0, Screen.height - scrollDistance, Screen.width, scrollDistance);
        Rect downRect = new Rect(0, 0, Screen.width, scrollDistance);

        desiredMove.x = leftRect.Contains(MouseInput) ? -1 : rightRect.Contains(MouseInput) ? 1 : 0;
        desiredMove.z = upRect.Contains(MouseInput) ? 1 : downRect.Contains(MouseInput) ? -1 : 0;

        if (Input.GetKey(KeyCode.W))
        {
            desiredMove.z = 1;
        }

        if (Input.GetKey(KeyCode.S))
        {
            desiredMove.z = -1;
        }

        if (Input.GetKey(KeyCode.A))
        {
            desiredMove.x = -1;
        }

        if (Input.GetKey(KeyCode.D))
        {
            desiredMove.x = 1;
        }


        desiredMove *= scrollSpeed;
        desiredMove *= Time.deltaTime;
        desiredMove = Quaternion.Euler(new Vector3(0f, transform.eulerAngles.y, 0f)) * desiredMove;
        desiredMove = transform.InverseTransformDirection(desiredMove);

        transform.Translate(desiredMove, Space.Self);
        HeightCalculation();
        if (Input.GetMouseButton(1))
        {
            rotationX += Input.GetAxis("Mouse X") * sensX;
            rotationX = Mathf.Clamp(rotationX, -100, 100);
            Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, groundMask.value))
            {
                transform.RotateAround(hit.point, new Vector3(0.0f, 1.0f, 0.0f), Time.deltaTime * rotationX);
            }
        }
    }

    public float heightDampening = 5f;
    public float scrollWheelZoomingSensitivity = 2f;
    private float zoomPos = 5f;

    private void HeightCalculation()
    {
        float distanceToGround = DistanceToGround();
        zoomPos -= ScrollWheel * Time.deltaTime * scrollWheelZoomingSensitivity;

        zoomPos = Mathf.Clamp01(zoomPos);

        float targetHeight = Mathf.Lerp(minHeight, maxHeight, zoomPos);
        float difference = 0;

        if (distanceToGround != targetHeight)
            difference = targetHeight - distanceToGround;

        transform.position = Vector3.Lerp(transform.position,
            new Vector3(transform.position.x, targetHeight + difference, transform.position.z),
            Time.deltaTime * heightDampening);
    }

    private float DistanceToGround()
    {
        Ray ray = new Ray(transform.position, Vector3.down);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, groundMask.value))
            return (hit.point - transform.position).magnitude;

        return 0f;
    }

    float rotationX = 0.0f;
}