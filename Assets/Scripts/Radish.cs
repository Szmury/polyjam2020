﻿using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;

public class Radish : MonoBehaviour
{
    [NonSerialized] public int damagePerSec = 25;
    public Animator Animator;
    public GameObject radishChild;
    private SphereCollider boxCollider;
    private bool counting = false;
    public List<Enemy> enemies = new List<Enemy>();
    private bool exploding = false;
    public ParticleSystem particle;

    public void Place()
    {
        transform.position = new Vector3(transform.position.x, 0, transform.position.z);
        radishChild.transform.localPosition = Vector3.zero;
        StatsHolder.Instance.SunPower -= BalanceHolder.RadishCost;
        Animator.SetTrigger("Place");
        StartCoroutine(WaitWithCollision());
    }

    private IEnumerator WaitWithCollision()
    {
        yield return new WaitForSeconds(2f);
        boxCollider = gameObject.AddComponent<SphereCollider>();
        boxCollider.isTrigger = true;
        boxCollider.radius = 5;
    }

    public void TriggerExplode()
    {
        if (!exploding)
        {
            exploding = true;
            Animator.SetTrigger("Explode");
            StartCoroutine(DelayExplosion());
        }
        
    }

    private IEnumerator DelayExplosion()
    {
        yield return new WaitForSeconds(0.35f);
        particle.Play();

    }

    public void Explode()
    {
        foreach (var enem in enemies)
        {
            if (enem != null)
            {
                enem.Hit(damagePerSec);    
            }
        }
        Destroy(gameObject);
    }
}
