﻿using System;
using UnityEngine;

public class StatsHolder : MonoBehaviour
{
    public float SunPower = 0;
    [NonSerialized] public float RootRanges = 50;
    
    private static StatsHolder _instance;

    public static StatsHolder Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        SunPower = 30;
    }

}
