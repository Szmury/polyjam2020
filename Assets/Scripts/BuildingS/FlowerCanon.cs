﻿using System.Collections;
using UnityEngine;

namespace DefaultNamespace
{
    public class FlowerCanon : MonoBehaviour
    {
        public GameObject bullet;
        private Animator animator;
        private Coroutine coroutine;

        private void Start()
        {
            animator = GetComponentInChildren<Animator>();
        }

        public void Shoot(Transform target)
        {
            animator?.SetTrigger("Shoot");

            GameObject spawnedBullet = Instantiate(bullet, transform.position, Quaternion.identity);
            spawnedBullet.GetComponent<Bullet>().target = target;
            spawnedBullet.transform.LookAt(target);

            coroutine = StartCoroutine(MoveBullet(spawnedBullet.transform));
        }

        private IEnumerator MoveBullet(Transform bullet)
        {
            var target = bullet.gameObject.GetComponent<Bullet>().target;
            while (PerfprmSafeCheck(bullet, target))
            {
                if (target != null)
                {
                    bullet.LookAt(target);
                }
                else
                {
                    break;
                }

                bullet.transform.position += bullet.transform.forward * 20 * Time.deltaTime;
                yield return null;
            }

            Destroy(bullet.gameObject);
        }

        private bool PerfprmSafeCheck(Transform bullet, Transform target)
        {
            return bullet.transform != null && target != null && 
                   bullet.position.y > -1 && Vector3.Distance(bullet.position, target.position) > 1;
        }
    }

}


