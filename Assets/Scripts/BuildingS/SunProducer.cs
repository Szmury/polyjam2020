﻿using UnityEngine;

namespace DefaultNamespace
{
    public class SunProducer : MonoBehaviour
    {
        private float time = 0;
        public float SunProduced = 0.5f;
        private BuildingScript buildingScript;
        
        private void Awake()
        {
            buildingScript = GetComponentInParent<BuildingScript>();
        }

        private void Update()
        {
            StatsHolder.Instance.SunPower += Time.deltaTime * SunProduced 
                                             + Time.deltaTime * SunProduced  * buildingScript.sunLevel;
        }
    }
}
