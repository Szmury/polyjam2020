﻿using UnityEngine;

namespace DefaultNamespace
{
    public class SunFlower : MonoBehaviour
    {
        public Animator animator;

        public void Hurt()
        {
            animator?.SetTrigger("Hurt");
        }

        public void Dead()
        {
            animator?.SetTrigger("Wither");
        }
    }
}
