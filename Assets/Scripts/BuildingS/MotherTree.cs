﻿using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MotherTree : MonoBehaviour
{
    public GameObject gamoOver;
    private static MotherTree _instance;

    public static MotherTree Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }


    private BuildingScript building;
    private bool once = false;
    
    private void Start()
    {
        building = gameObject.GetComponent<BuildingScript>();
    }

    public void Update()
    {
        if (once)
            return;
        
        
        if (building.HitPoints <= 0 )
        {
            once = true;
            StartCoroutine(DislayGameOver());
        }
        
    }

    private IEnumerator DislayGameOver()
    {
        yield return new WaitForSeconds(5f);
        gamoOver.SetActive(true);
        yield return new WaitForSeconds(5f);
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
}