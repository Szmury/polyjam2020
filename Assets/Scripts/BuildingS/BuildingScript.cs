﻿using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using System.Linq;

public class BuildingScript : MonoBehaviour
{
    public Renderer renderer;
    public FlowerTower flowerTower;
    public GameObject FlowerTowerRoot;
    public FlowerCanon[] FlowerCannons;
    public SunFlower[] sunFlowers;
    public BuildingState state = BuildingState.Virgin;
    public float regen = 1f;
    public Animator Animator;
    public GameObject gameObject;
    public GameObject Circle;

    public MeshRenderer cheatMesh;

    [NonSerialized]
    public int sunLevel = 1;
    [NonSerialized]
    public float towerLevel = 1;
    [NonSerialized]
    public float AtackSpeedLevel = 1;

    [NonSerialized] 
    public float TowerRange = 1;

    private SphereCollider boxCollider;
    [NonSerialized] public float boxSize;
    public GameObject LifeBar;
    public List<GameObject> fires;
    
    [SerializeField] public float HitPoints;
    public float MaxHelth = 50f; 
    
    private void Awake()
    {
        boxCollider = GetComponent<SphereCollider>();
        boxSize = boxCollider.radius;
        boxCollider.isTrigger = true;
        HitPoints = MaxHelth;

        
        FlowerCannons.ToList().ForEach(x => x.gameObject.SetActive(false));
        sunFlowers.ToList().ForEach(x => x.gameObject.SetActive(false));

        if (cheatMesh != null)
            cheatMesh.enabled = false;
    }

    private void Update()
    {
        if (fires.Count > 0)
        {
            fires[0].gameObject.SetActive(HitPoints/ MaxHelth < 0.80f);
            fires[1].gameObject.SetActive(HitPoints/ MaxHelth < 0.60f);
            fires[2].gameObject.SetActive(HitPoints/ MaxHelth < 0.40f);
            fires[3].gameObject.SetActive(HitPoints/ MaxHelth < 0.25f);
        }
        
        Circle.gameObject.SetActive((state == BuildingState.Virgin && Vector3.Distance(
                transform.position, Vector3.zero) <= StatsHolder.Instance.RootRanges/2));
   
        
        HitPoints = HitPoints + Time.deltaTime * regen;
        if (HitPoints > MaxHelth)
        {
            HitPoints = MaxHelth;
        }
    }

    public void ClearSelection()
    {
        renderer.material.color = Color.white;
    }

    public void Select()
    {
        renderer.material.color = new Color(240 / 255, 255 / 255, 255 / 255);
        UiScript.Instance.ShowButtons(state);
    }

    public void SetInfestedState()
    {
        if (state == BuildingState.Virgin)
        {
            StatsHolder.Instance.SunPower -= BalanceHolder.InfestedCost;
            state = BuildingState.Infested;
            RootRange.Instance.IncreaseRange();
            LifeBar.SetActive(true);

            boxCollider.radius = 12f;
            Animator?.SetTrigger("Emergetrigger");
        }
    }

    public void SetFlowerState()
    {
        if (state != BuildingState.Virgin)
        {
            TryToDestroy<FlowerTower>(FlowerTowerRoot);
            FlowerTowerRoot.SetActive(false);
            StatsHolder.Instance.SunPower -= BalanceHolder.flowerCost;
            gameObject.AddComponent<SunProducer>();
            sunFlowers.ToList().ForEach(x => x.gameObject.SetActive(true));
            FlowerCannons.ToList().ForEach(x => x.gameObject.SetActive(false));
            state = BuildingState.Sunflower;
        }
    }
   
    public void SetTowerState()
    {
        if (state != BuildingState.Virgin)
        {
            TryToDestroy<SunProducer>(gameObject);
            
            FlowerTowerRoot.SetActive(true);
            StatsHolder.Instance.SunPower -= BalanceHolder.TowerCost;
            flowerTower = FlowerTowerRoot.AddComponent<FlowerTower>();
            state = BuildingState.Tower;
            sunFlowers.ToList().ForEach(x => x.gameObject.SetActive(false));
            FlowerCannons.ToList().ForEach(x => x.gameObject.SetActive(true));
            flowerTower.FlowerCanons = FlowerCannons;
        }
    }

    public void UpgradeSunGeneration()
    {
        StatsHolder.Instance.SunPower -= BalanceHolder.flowerCost * sunLevel;
        sunLevel += 1;
    }

    public void UpgradeTower()
    {
        StatsHolder.Instance.SunPower -= BalanceHolder.TowerCost * towerLevel;
        towerLevel += 1;
    }
    
    public void UpgradeAtacksSpeed()
    {
        StatsHolder.Instance.SunPower -= BalanceHolder.TowerCost * AtackSpeedLevel;
        AtackSpeedLevel += 1;
    }

    public void UpgradeTowerRange()
    {
        StatsHolder.Instance.SunPower -= BalanceHolder.TowerCost * TowerRange;
        TowerRange += 1;
        flowerTower.UpgradeRange();
    }
    
    private void TryToDestroy<T>(GameObject gj) where T : MonoBehaviour
    {
        T sunProducer = gj.GetComponent<T>();
        if(sunProducer != null)
            Destroy(sunProducer);
    }
    
    public bool Hit(float dmg)
    {
        if (state == BuildingState.Virgin)
            return true;

        if (state == BuildingState.Sunflower)
        {
            sunFlowers.ToList().ForEach(x => x.Hurt());
        } 

        HitPoints-= dmg;
        if (HitPoints<=0 && Animator != null)
        {
            Animator?.SetTrigger("Dead");

            if (state == BuildingState.Sunflower)
            {
                sunFlowers.ToList().ForEach(x => x.Dead());
            }
            Reset();
        }

        return HitPoints <= 0;
    }

    private void Reset()
    {
        if (this.GetComponent<MotherTree>()!=null)
        {
            return;
        }

        TryToDestroy<FlowerTower>(FlowerTowerRoot);
        TryToDestroy<SunProducer>(gameObject);
        sunLevel = 1;
        towerLevel = 1;
        AtackSpeedLevel = 1;
        TowerRange = 1;
        regen = 1;
        HitPoints = MaxHelth;
        state = BuildingState.Virgin;
        LifeBar.SetActive(false);
        RootRange.Instance.DecreaseScale();
    }

    public void UpgradeRegen()
    {
        StatsHolder.Instance.SunPower -= BalanceHolder.TowerRegenCost * regen;
        regen += 1;
    }
}


public enum BuildingState
{
    Virgin,
    Infested,
    Sunflower,
    Tower,
    MotherTree
    
}