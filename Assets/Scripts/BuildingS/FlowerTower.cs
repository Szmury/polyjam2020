﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace DefaultNamespace
{
    public class FlowerTower : MonoBehaviour
    {
        public FlowerCanon[] FlowerCanons;
        public SunFlower[] Sunflowers;
        public List<Enemy> AvailableTargets = new List<Enemy>();
        private BuildingScript buildingScript;
        public SphereCollider BoxCollider;
        
        private float time = 0;

        private void Awake()
        {
            buildingScript = gameObject.GetComponentInParent<BuildingScript>();
            BoxCollider = gameObject.AddComponent<SphereCollider>();
            BoxCollider.isTrigger = true;
            BoxCollider.radius = 12f;
            BoxCollider.center = Vector3.zero;
        }

        public void UpgradeRange()
        {
            BoxCollider.radius = 12 + (buildingScript.TowerRange * 2);
        }

        public void Update()
        {
            time += Time.deltaTime;
            if (time > BalanceHolder.TowerShootSpeed * Mathf.Pow(0.95f,buildingScript.AtackSpeedLevel) && AvailableTargets.Count > 0)
            {
                
                Enemy lowestHpEnemy = null;
                float enemyHp = float.PositiveInfinity;

                AvailableTargets.RemoveAll(enemy => enemy == null || enemy.isDead);

                foreach (var enemy in AvailableTargets)
                {
                    if (enemy.hitPoints < enemyHp)
                    {
                        lowestHpEnemy = enemy;
                        enemyHp = enemy.hitPoints;
                    }
                }
                
                if(lowestHpEnemy == null)
                    return;

                FlowerCanon closest = FlowerCanons[0];
                float Distance = float.MaxValue;
                foreach (var canon in FlowerCanons)
                {
                    if (Vector3.Distance(canon.transform.position, lowestHpEnemy.transform.position) <= Distance)
                    {
                        Distance = Vector3.Distance(canon.transform.position, lowestHpEnemy.transform.position);
                        closest = canon;
                    }
                }
                
                closest.Shoot(lowestHpEnemy.transform);
                time = 0;
 
                lowestHpEnemy?.Hit(buildingScript.towerLevel);
            }
        }
        
        public void AddEnemy(Enemy enemy)
        {
            if(AvailableTargets.Contains(enemy))
                return;
            AvailableTargets.Add(enemy);
        }

        public void RemoveEnemy(Enemy enemy)
        {
//            if(AvailableTargets.Contains(enemy))
//                AvailableTargets.Remove(enemy);
        }
    }
}


