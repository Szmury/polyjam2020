﻿namespace DefaultNamespace
{
    public static class BalanceHolder
    {
        public const float InfestedCost = 5; //5s
        public const float flowerCost = 15; // 15
        public const float TowerCost = 10; //15

        public const float TowerShootSpeed = 1.2f;
        public const float TowerRegenCost = 5;
        public const float RadishCost = 5;
    }
}