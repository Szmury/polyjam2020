﻿using UnityEngine;

public class StreetTileSelector : MonoBehaviour
{
    private static StreetTileSelector _instance;
    public LayerMask groundLayer;

    public GameObject radishTile;
    public static StreetTileSelector Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    private GameObject radish;
   
    private void OnEnable()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 1000.0f, groundLayer.value))  
        {
            radish = Instantiate(radishTile, hit.point + Vector3.one, Quaternion.identity);
        }
    }

    public void Update()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 1000.0f, groundLayer.value))
        {
            radish.transform.position = hit.point + Vector3.one;
        }

        if (Input.GetMouseButtonDown(0))
        {
            radish.GetComponent<Radish>().Place();
            gameObject.GetComponent<BuildingSelector>().enabled = true;
            gameObject.GetComponent<StreetTileSelector>().enabled = false;
        }
    }
}
