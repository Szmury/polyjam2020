﻿using DefaultNamespace;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class UiScript : MonoBehaviour
{
    private static UiScript _instance;

    public static UiScript Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        TurnOffButtons();

        InfestButton.onClick.AddListener(() =>
        {
            BuildingSelector.Instance.selectedBuilding?.SetInfestedState();
            ShowButtons(BuildingSelector.Instance.selectedBuilding.state);
        });
        
        FlowerButton.onClick.AddListener(() =>
        {
            BuildingSelector.Instance.selectedBuilding?.SetFlowerState();
            ShowButtons(BuildingSelector.Instance.selectedBuilding.state);
        });
        
        FloweSunPowerButton.onClick.AddListener(() =>
        {
            BuildingSelector.Instance.selectedBuilding?.UpgradeSunGeneration();

        });
        
        TowerButton.onClick.AddListener(() =>
        {
            BuildingSelector.Instance.selectedBuilding?.SetTowerState();
            ShowButtons(BuildingSelector.Instance.selectedBuilding.state);
        });
        
        TowerUpgradeButton.onClick.AddListener(() =>
        {
            BuildingSelector.Instance.selectedBuilding?.UpgradeTower();
        });
        
        TowerSpeedUpgradeButton.onClick.AddListener(() =>
        {
            BuildingSelector.Instance.selectedBuilding?.UpgradeAtacksSpeed();
        });

        TowerRangeButton.onClick.AddListener(() =>
        {
            BuildingSelector.Instance.selectedBuilding?.UpgradeTowerRange();
        });
        
        RegenButton.onClick.AddListener(() =>
        {
            BuildingSelector.Instance.selectedBuilding?.UpgradeRegen();
            ShowButtons(BuildingSelector.Instance.selectedBuilding.state);
        });

        RadishButton.onClick.AddListener(() =>
        {
            BuildingSelector.Instance.gameObject.GetComponent<BuildingSelector>().enabled = false;
            StreetTileSelector.Instance.gameObject.GetComponent<StreetTileSelector>().enabled = true;
        });

        InfestButtonText.text = "" + BalanceHolder.InfestedCost;
        TowerButtonText.text = "" + BalanceHolder.TowerCost;
        RadishButtonText.text = "" + BalanceHolder.RadishCost;

    }
    private void DisableObject(Button obj)
    {
        obj.gameObject.SetActive(false);
        obj.gameObject.transform.parent.gameObject.SetActive(false);
    }

    private void TurnOffButtons()
    {
        var buttons = new[]
        {
            TowerRangeButton,
            TowerSpeedUpgradeButton,
            TowerUpgradeButton,
            FloweSunPowerButton,
            InfestButton,
            TurnipButton,
            FlowerButton,
            TowerButton,
            RegenButton,
        };

        buttons.ToList().ForEach(DisableObject);
    }

    public void Update()
    {
        var selectedBuilding = BuildingSelector.Instance.selectedBuilding;
        
        if (selectedBuilding != null)
        {
            InfestButton.interactable = StatsHolder.Instance.SunPower >= BalanceHolder.InfestedCost && IsBuildingInRange();

            RegenButton.interactable = StatsHolder.Instance.SunPower >= BalanceHolder.TowerRegenCost * selectedBuilding.regen;
            RadishButton.interactable = StatsHolder.Instance.SunPower >= BalanceHolder.RadishCost && IsRadishInRange();
            FlowerButton.interactable = StatsHolder.Instance.SunPower >= BalanceHolder.flowerCost;
            FloweSunPowerButton.interactable = StatsHolder.Instance.SunPower >= BalanceHolder.flowerCost * selectedBuilding.sunLevel;
            TowerButton.interactable = StatsHolder.Instance.SunPower >= BalanceHolder.TowerCost;
            TowerUpgradeButton.interactable = StatsHolder.Instance.SunPower >= BalanceHolder.TowerCost * selectedBuilding.towerLevel;
            TowerSpeedUpgradeButton.interactable = StatsHolder.Instance.SunPower >= BalanceHolder.TowerCost * selectedBuilding.AtackSpeedLevel;
            TowerRangeButton.interactable = StatsHolder.Instance.SunPower >= BalanceHolder.TowerCost * selectedBuilding.TowerRange;
            
            buildingHitPoints.text = ""+ ((int)selectedBuilding.HitPoints).ToString();
            buildingRegen.text = "" + ((int)selectedBuilding.regen).ToString();
            
            FloweSunPowerText.text = (BalanceHolder.flowerCost * selectedBuilding.sunLevel).ToString() ;
            FlowerButtonText.text = ""+ BalanceHolder.flowerCost;
            RegenButtonText.text = "" + BalanceHolder.TowerRegenCost * selectedBuilding.regen;
            TowerUpgradeText.text = "" + BalanceHolder.TowerCost * selectedBuilding.towerLevel;
            TowerSpeedButtonText.text = "" + BalanceHolder.TowerCost * selectedBuilding.AtackSpeedLevel;
            TowerRangeText.text = "" + BalanceHolder.TowerCost * selectedBuilding.TowerRange;
        }
        else
        {
            buildingHitPoints.text = null;
            buildingRegen.text = null;
        }

        sunPower.text = ((int)StatsHolder.Instance.SunPower).ToString();
    }

    //TODO: radish only in range
    private bool IsRadishInRange()
    {
        return true;
    }

    private bool IsBuildingInRange()
    {
        return StatsHolder.Instance.RootRanges / 2 >= Vector3.Distance(Vector3.zero,
                   new Vector3(BuildingSelector.Instance.selectedBuilding.transform.position.x, 0,
                       BuildingSelector.Instance.selectedBuilding.transform.position.z));
    }

    public Button InfestButton;
    public Button FlowerButton;
    public Button FloweSunPowerButton;
    public Button TurnipButton;
    public Button TowerButton;
    public Button TowerUpgradeButton;
    public Button TowerSpeedUpgradeButton;
    public Button TowerRangeButton;
    public Button RegenButton;
    public Button RadishButton;
    

    public TextMeshProUGUI InfestButtonText;
    public TextMeshProUGUI FlowerButtonText;
    public TextMeshProUGUI TurnipButtonText;
    public TextMeshProUGUI TowerButtonText;
    public TextMeshProUGUI TowerSpeedButtonText;
    public TextMeshProUGUI RegenButtonText;
    public TextMeshProUGUI RadishButtonText;
    public TextMeshProUGUI FloweSunPowerText;
    public TextMeshProUGUI TowerUpgradeText;
    public TextMeshProUGUI TowerRangeText;

    public GameObject buildingPanel;

    

    public TextMeshProUGUI sunPower;

    public TextMeshProUGUI buildingHitPoints;
    public TextMeshProUGUI buildingRegen;



    public void EnableObject(Button button)
    {
        button.gameObject.SetActive(true);
        button.transform.parent.gameObject.SetActive(true);

    }
    public void ShowButtons(BuildingState state)
    {
        TurnOffButtons();

        EnableObject(TurnipButton);
        if (state == BuildingState.Virgin)
        {
            EnableObject(InfestButton);
        }
        if (state == BuildingState.Infested)
        {
            EnableObject(FlowerButton);
            EnableObject(TowerButton);
        }

        if (state != BuildingState.Virgin)
        {
            EnableObject(RegenButton);
        }

        if (state == BuildingState.Sunflower)
        {
            EnableObject(FloweSunPowerButton);
            EnableObject(TowerButton);
        }

        if (state == BuildingState.Tower)
        {
            EnableObject(FlowerButton);
            EnableObject(TowerUpgradeButton);
            EnableObject(TowerSpeedUpgradeButton);
            EnableObject(TowerRangeButton);
        }
    }
}