﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeBar : MonoBehaviour
{
    public BuildingScript building;

    public Image image;

    public RectTransform Imagecanvas;

    void Update()
    {
        gameObject.transform.LookAt(CameraSingleton.Instance.transform);
        image.color = Color.Lerp(Color.red, Color.green, building.HitPoints / building.MaxHelth);
        Imagecanvas.sizeDelta = new Vector2(building.HitPoints / building.MaxHelth * 10, 0.5f);
    }
}
