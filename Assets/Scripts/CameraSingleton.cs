﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSingleton : MonoBehaviour
{
    private static CameraSingleton _instance;

    public static CameraSingleton Instance
    {
        get { return _instance; }
    }

    public Camera Camera;
    

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            Camera = GetComponent<Camera>();
        }
    }
}
