﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GridBuildingSpawner : MonoBehaviour
{
    public GameObject building;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = -30; i <= 30; i+=10)
        {
            for (int j = -30; j <= 30; j+=10)
            {
                var obj = Instantiate(building, new Vector3(i, -1, j), Quaternion.identity);
                obj.transform.SetParent(ObstacleAdd.Instance.gameObject.transform);
                obj.gameObject.AddComponent(typeof(NavMeshObstacle));

                var navMeshObstacle = obj.GetComponent<NavMeshObstacle>();


                navMeshObstacle.shape = NavMeshObstacleShape.Box;
                navMeshObstacle.carving = true;
                navMeshObstacle.size = obj.transform.localScale;
                navMeshObstacle.center = obj.transform.position;
            }
        }

        GetComponent<ObstacleAdd>().enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}