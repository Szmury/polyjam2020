﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class BuildingSelector : MonoBehaviour
{
    private static BuildingSelector _instance;

    public static BuildingSelector Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public BuildingScript selectedBuilding;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.IsPointerOverGameObject())
                return;
            
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 1000.0f))
            {
                BuildingScript buildingScript = hit.transform.gameObject.GetComponent<BuildingScript>();
                if (buildingScript != null)
                {
                    ClearPreviousBuilding();
                    selectedBuilding = hit.transform.gameObject.GetComponent<BuildingScript>();
                    selectedBuilding.Select();
                    UiScript.Instance.buildingPanel.SetActive(true);
                }
            }
        }
    }

    private void ClearPreviousBuilding()
    {
        if (selectedBuilding != null)
        {
            selectedBuilding.ClearSelection();
            UiScript.Instance.buildingPanel.SetActive(false);
        }
    }
}
