﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RootRange : MonoBehaviour
{
    private static RootRange _instance;
    public static RootRange Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public void IncreaseRange()
    {
        StatsHolder.Instance.RootRanges += 5;
    }

    public void DecreaseScale()
    {
        StatsHolder.Instance.RootRanges -= 5;
    }
    
    // Update is called once per frame
    void Update()
    {
        BuildingScript script = BuildingSelector.Instance.selectedBuilding;
        if (script != null && script.state == BuildingState.Tower)
        {
            float size = script.flowerTower.BoxCollider.radius;
            this.transform.localScale = new Vector3(size * 2, 0.01f, size * 2);
            this.transform.position = new Vector3(script.transform.position.x, 0.05f, script.transform.position.z);
            
        }
        else
        {
            this.transform.localScale = new Vector3(StatsHolder.Instance.RootRanges, 0.01f, StatsHolder.Instance.RootRanges);
            this.transform.position = new Vector3(0, 0.05f, 0);
        }
    }
}
