﻿using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class Spawner : MonoBehaviour
{
    private float radius;

    [SerializeField]
    private float UpgradeTime;
    [SerializeField] 
    private float spawnRate;
    private float countdownToSpawn;
    private float countdownToUpgrade;
    private int upgradeCount = 1;
    private int maxUpp = 1;

    public TextMeshProUGUI EnemiesGrowStronger;

    [SerializeField] 
    private List<GameObject> enemyPrefab;

    [SerializeField]
    public GameObject[] SpawnPoints;

    void Start()
    {
        radius = transform.localScale.x;
        countdownToSpawn = spawnRate;
    }

    void Update()
    {
        countdownToSpawn = countdownToSpawn - Time.deltaTime;
        if (countdownToSpawn < 0.0f)
        {
            countdownToSpawn = spawnRate;
            Spawn();
        }

        countdownToUpgrade += Time.deltaTime;
        if (countdownToUpgrade >= UpgradeTime)
        {
            countdownToUpgrade = 0;
            upgradeCount += 1;
            maxUpp += 1;
            spawnRate = spawnRate * 0.85f;
            if (upgradeCount > 15)
            {
                upgradeCount = 15;
            }
            EnemiesGrowStronger.DOKill();
            EnemiesGrowStronger.color = Color.red;
            DOTween.Sequence().Append(
                    EnemiesGrowStronger.DOColor(Color.red, 0.5f))
                .Append(EnemiesGrowStronger.DOColor(new Color(1, 0, 0, 0), 1f)).Play();
        }
    }

    private void Spawn()
    {
        var nextSpawnPoint = Random.Range(0,SpawnPoints.Length);

        var d = SpawnPoints[nextSpawnPoint];

        var enemy = Instantiate(GetPrefab(), d.transform.position, Quaternion.identity).GetComponent<Enemy>();
        enemy.hitPoints += maxUpp;
        if (upgradeCount < 15)
        {
            enemy.Damage += ((maxUpp - 15f) / 15f);
        }
        enemy.GetComponent<NavMeshAgent>().speed += maxUpp / 2f;
    }

    private GameObject GetPrefab()
    {
        float ranomd = Random.RandomRange(0, 100);
        if (ranomd > 130 - 4 * upgradeCount)
        {
            return enemyPrefab[2];
        }
        if (ranomd >= 90 - 4 * upgradeCount)
        {
            return enemyPrefab[1];
        }

        return enemyPrefab[0];

    }
    
    
}
