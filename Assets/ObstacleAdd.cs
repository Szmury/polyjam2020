﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ObstacleAdd : MonoBehaviour
{
    private static ObstacleAdd _instance;

    public static ObstacleAdd Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            var child = transform.GetChild(i);
            child.gameObject.AddComponent(typeof(NavMeshObstacle));
            child.gameObject.AddComponent(typeof(Rigidbody));

            var navMeshObstacle = child.GetComponent<NavMeshObstacle>();

            navMeshObstacle.shape = NavMeshObstacleShape.Box;
            navMeshObstacle.carving = true;
            navMeshObstacle.size = child.localScale;
            navMeshObstacle.center = child.position;

            var rigidBody = child.GetComponent<Rigidbody>();
            rigidBody.useGravity = false;
            rigidBody.mass = 0;
            rigidBody.isKinematic = false;
            rigidBody.constraints = RigidbodyConstraints.FreezeAll;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
