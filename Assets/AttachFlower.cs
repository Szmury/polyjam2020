﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachFlower : MonoBehaviour
{
    public GameObject flower;
    public GameObject attachTo;
    public GameObject meshRenderer;

    // Start is called before the first frame update
    void Start()
    {

        var size = meshRenderer.GetComponent<SkinnedMeshRenderer>().bounds.size;
        var y =  size.y / 2;
        var x = 0;
        var z = size.z / 2;

        var rotations = new int[4]
        {
            -45, 135, 45, 225
        };

        var zets = new float[4]
        {
            size.z/2, -size.z/2,0,0
        };

        var xes = new float[4]
        {
            0,0,size.x/2,-size.x/2
        };


        for (int i = 0; i < 4; i++)
        {
            var obj = Instantiate(flower, new Vector3(xes[i], y, zets[i]), Quaternion.Euler(0, rotations[i], 0), attachTo.transform);

            obj.transform.localPosition = new Vector3(obj.transform.position.x, obj.transform.position.y, obj.transform.position.z);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
