﻿using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using DG.Tweening;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    [SerializeField] GameObject destination;
    private GameObject target;

    private Coroutine cor;

    [SerializeField] private float fireRate;
    private float countdownToFire;
    public Animator Animator;

    [SerializeField] public float hitPoints = 2;

    [SerializeField] public float Damage = 1;
    public NavMeshAgent navMeshAgent;

    public bool isDead;
    public ParticleSystem partciles;
    public List<GameObject> Fires;

    // Start is called before the first frame update
    void Start()
    {
        navMeshAgent.destination = Vector3.zero;
        partciles.Play();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!navMeshAgent.isStopped)
        {
            navMeshAgent.isStopped = true;
        }

        transform.LookAt(other.transform);
        var building = other.gameObject.GetComponent<BuildingScript>();

        if (building != null)
        {
            if (building.state != BuildingState.Virgin)
            {
                target = other.gameObject;
                return;
            }
        }

        var tower = other.gameObject.GetComponent<FlowerTower>();
        if (tower != null)
        {
            tower.AddEnemy(this);
            return;
        }

        var radish = other.gameObject.GetComponent<Radish>();
        if (radish != null)
        {
            if (cor == null)
            {
                radish.TriggerExplode();
                cor = StartCoroutine(AoeDamage(radish));
            }

            radish.enemies.Add(this);
        }
    }

    public IEnumerator AoeDamage(Radish radish)
    {
        yield return new WaitForSeconds(0.70f);
        radish.Explode();
        cor = null;
    }

    private void OnTriggerExit(Collider other)
    {
        var building = other.gameObject.GetComponent<BuildingScript>();
        if (building != null)
        {
            if (building.state == BuildingState.Tower)
            {
                building.flowerTower.RemoveEnemy(this);
            }


            return;
        }

        var tower = other.gameObject.GetComponent<FlowerTower>();
        if (tower != null)
        {
            tower.RemoveEnemy(this);
        }
    }

    public void Hit(float power)
    {
        if (Animator != null)
        {
            Animator.SetTrigger("Hurt");
        }

        hitPoints -= power;
        if (hitPoints < 0)
        {
            if (Animator != null)
            {
                partciles.Stop();
                Animator.SetTrigger("Dead");
            }

            StartCoroutine(clearBody());
            navMeshAgent.isStopped = false;
            StartCoroutine(clearBody());
            isDead = true;
        }
    }

    private IEnumerator clearBody()
    {
        if(gameObject.tag != "car")
            yield return new WaitForSeconds(3);
        Destroy(gameObject);
    }

    public void Hit()
    {
        Hit(1);
    }

    // Update is called once per frame
    void Update()
    {
        if (isDead)
        {
            navMeshAgent.isStopped = true;
            return;
        }

        if (target == null)
        {
            if (Animator != null && partciles.isPlaying)
            {
                partciles.Stop();
                Animator.SetBool("Fire", false);
                Animator.SetBool("Run", true);
                Animator.speed = navMeshAgent.speed / 2;
            }

            navMeshAgent.isStopped = false;
        }

        if (target != null)
        {
            if (Animator != null && !partciles.isPlaying)
            {
                partciles.Play();
                Animator.speed = 1;
                Animator.SetBool("Run", false);
                Animator.SetBool("Fire", true);
            }

            countdownToFire = countdownToFire - Time.deltaTime;
            if (countdownToFire < 0.0f)
            {
                transform.LookAt(target.transform.position);
                countdownToFire = fireRate;
                if (target.GetComponent<BuildingScript>().Hit(Damage))
                    target = null;
            }
        }
    }
}